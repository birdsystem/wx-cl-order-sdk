<?php

namespace WarehouseX\ClOrder;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public static array $types = [
        'getInboundOrderDetailCollection' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrderDetail[]',
        ],
        'getInboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrderDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrder\\InboundOrderOutput[]',
        ],
        'postInboundOrderCollection' => [
            '201.' => 'WarehouseX\\ClOrder\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteInboundOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\InboundOrderDetail[]',
        ],
        'getOutboundOrderDetailCollection' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrderDetail[]',
        ],
        'getOutboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrderDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrder\\OutboundOrderOutput[]',
        ],
        'postOutboundOrderCollection' => [
            '201.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteOutboundOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource' => [
            '200.' => 'WarehouseX\\ClOrder\\Model\\OutboundOrderDetail[]',
        ],
    ];
}
