<?php

namespace WarehouseX\ClOrder\Api;

use OpenAPI\Runtime\AbstractAPI as BaseClass;
use WarehouseX\ClOrder\ResponseHandlerStack;

class AbstractAPI extends BaseClass implements APIInterface
{
    protected string $responseHandlerStackClass = ResponseHandlerStack::class;

    public function __construct(?HttpClientInterface $client = null)
    {
        parent::__construct($client);
    }
}
