<?php

namespace WarehouseX\ClOrder\Api;

use WarehouseX\ClOrder\Model\OutboundOrder\OutboundOrderInput as OutboundOrderInput;
use WarehouseX\ClOrder\Model\OutboundOrder\OutboundOrderInput\OutboundOrderPut as OutboundOrderPut;
use WarehouseX\ClOrder\Model\OutboundOrder\OutboundOrderOutput as OutboundOrderOutput;
use WarehouseX\ClOrder\Model\OutboundOrder\OutboundOrderOutput\OutboundOrderGetDetail as OutboundOrderGetDetail;

class OutboundOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'orderNumber'	string
     *                       'trackingNumber'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'companyName'	string
     *                       'telephone'	string
     *                       'postCode'	string
     *                       'city'	string
     *                       'county'	string
     *                       'countryIso'	string
     *                       'countryIso[]'	array
     *                       'clientId[between]'	string
     *                       'clientId[gt]'	string
     *                       'clientId[gte]'	string
     *                       'clientId[lt]'	string
     *                       'clientId[lte]'	string
     *                       'warehouseId[between]'	string
     *                       'warehouseId[gt]'	string
     *                       'warehouseId[gte]'	string
     *                       'warehouseId[lt]'	string
     *                       'warehouseId[lte]'	string
     *                       'logisticsServiceId[between]'	string
     *                       'logisticsServiceId[gt]'	string
     *                       'logisticsServiceId[gte]'	string
     *                       'logisticsServiceId[lt]'	string
     *                       'logisticsServiceId[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundOrderOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundOrderCollection',
        'GET',
        'api/cl-order/outbound_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a OutboundOrder resource.
     *
     * @param OutboundOrderInput $Model The new OutboundOrder resource
     *
     * @return OutboundOrderGetDetail
     */
    public function postCollection(OutboundOrderInput $Model): OutboundOrderGetDetail
    {
        return $this->request(
        'postOutboundOrderCollection',
        'POST',
        'api/cl-order/outbound_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundOrderGetDetail|null
     */
    public function getItem(string $id): ?OutboundOrderGetDetail
    {
        return $this->request(
        'getOutboundOrderItem',
        'GET',
        "api/cl-order/outbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutboundOrder resource.
     *
     * @param string           $id    Resource identifier
     * @param OutboundOrderPut $Model The updated OutboundOrder resource
     *
     * @return OutboundOrderGetDetail
     */
    public function putItem(string $id, OutboundOrderPut $Model): OutboundOrderGetDetail
    {
        return $this->request(
        'putOutboundOrderItem',
        'PUT',
        "api/cl-order/outbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the OutboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteOutboundOrderItem',
        'DELETE',
        "api/cl-order/outbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the OutboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundOrderGetDetail
     */
    public function patchItem(string $id): OutboundOrderGetDetail
    {
        return $this->request(
        'patchOutboundOrderItem',
        'PATCH',
        "api/cl-order/outbound_orders/$id",
        null,
        [],
        []
        );
    }
}
