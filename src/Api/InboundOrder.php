<?php

namespace WarehouseX\ClOrder\Api;

use WarehouseX\ClOrder\Model\InboundOrder\InboundOrderInput\InboundOrderInput\InboundOrderPost as InboundOrderPost;
use WarehouseX\ClOrder\Model\InboundOrder\InboundOrderInput\InboundOrderInput\InboundOrderPut as InboundOrderPut;
use WarehouseX\ClOrder\Model\InboundOrder\InboundOrderOutput as InboundOrderOutput;
use WarehouseX\ClOrder\Model\InboundOrder\InboundOrderOutput\InboundOrderGetDetail as InboundOrderGetDetail;

class InboundOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'orderNumber'	string
     *                       'trackingNumber'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'warehouseId[between]'	string
     *                       'warehouseId[gt]'	string
     *                       'warehouseId[gte]'	string
     *                       'warehouseId[lt]'	string
     *                       'warehouseId[lte]'	string
     *                       'estimatedCartonQuantity[between]'	string
     *                       'estimatedCartonQuantity[gt]'	string
     *                       'estimatedCartonQuantity[gte]'	string
     *                       'estimatedCartonQuantity[lt]'	string
     *                       'estimatedCartonQuantity[lte]'	string
     *                       'actualCartonQuantity[between]'	string
     *                       'actualCartonQuantity[gt]'	string
     *                       'actualCartonQuantity[gte]'	string
     *                       'actualCartonQuantity[lt]'	string
     *                       'actualCartonQuantity[lte]'	string
     *                       'stockedCartonQuantity[between]'	string
     *                       'stockedCartonQuantity[gt]'	string
     *                       'stockedCartonQuantity[gte]'	string
     *                       'stockedCartonQuantity[lt]'	string
     *                       'stockedCartonQuantity[lte]'	string
     *                       'estimatedArrivalTime[before]'	string
     *                       'estimatedArrivalTime[strictly_before]'	string
     *                       'estimatedArrivalTime[after]'	string
     *                       'estimatedArrivalTime[strictly_after]'	string
     *                       'lastReceiveTime[before]'	string
     *                       'lastReceiveTime[strictly_before]'	string
     *                       'lastReceiveTime[after]'	string
     *                       'lastReceiveTime[strictly_after]'	string
     *                       'lastStockedTime[before]'	string
     *                       'lastStockedTime[strictly_before]'	string
     *                       'lastStockedTime[after]'	string
     *                       'lastStockedTime[strictly_after]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[orderNumber]'	string
     *                       'order[clientId]'	string
     *                       'order[warehouseId]'	string
     *                       'order[estimatedCartonQuantity]'	string
     *                       'order[actualCartonQuantity]'	string
     *                       'order[stockedCartonQuantity]'	string
     *                       'order[estimatedArrivalTime]'	string
     *                       'order[lastReceiveTime]'	string
     *                       'order[lastStockedTime]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[status]'	string
     *
     * @return InboundOrderOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderCollection',
        'GET',
        'api/cl-order/inbound_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a InboundOrder resource.
     *
     * @param InboundOrderPost $Model The new InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function postCollection(InboundOrderPost $Model): InboundOrderGetDetail
    {
        return $this->request(
        'postInboundOrderCollection',
        'POST',
        'api/cl-order/inbound_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderGetDetail|null
     */
    public function getItem(string $id): ?InboundOrderGetDetail
    {
        return $this->request(
        'getInboundOrderItem',
        'GET',
        "api/cl-order/inbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @param string          $id    Resource identifier
     * @param InboundOrderPut $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function putItem(string $id, InboundOrderPut $Model): InboundOrderGetDetail
    {
        return $this->request(
        'putInboundOrderItem',
        'PUT',
        "api/cl-order/inbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the InboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteInboundOrderItem',
        'DELETE',
        "api/cl-order/inbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the InboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderGetDetail
     */
    public function patchItem(string $id): InboundOrderGetDetail
    {
        return $this->request(
        'patchInboundOrderItem',
        'PATCH',
        "api/cl-order/inbound_orders/$id",
        null,
        [],
        []
        );
    }
}
