<?php

namespace WarehouseX\ClOrder\Api;

use WarehouseX\ClOrder\Model\OutboundOrderDetail as OutboundOrderDetailModel;

class OutboundOrderDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundOrderDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'status'	string
     *                       'status[]'	array
     *                       'cartonId[between]'	string
     *                       'cartonId[gt]'	string
     *                       'cartonId[gte]'	string
     *                       'cartonId[lt]'	string
     *                       'cartonId[lte]'	string
     *                       'quantity[between]'	string
     *                       'quantity[gt]'	string
     *                       'quantity[gte]'	string
     *                       'quantity[lt]'	string
     *                       'quantity[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundOrderDetailModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundOrderDetailCollection',
        'GET',
        'api/cl-order/outbound_order_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a OutboundOrderDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundOrderDetailModel|null
     */
    public function getItem(string $id): ?OutboundOrderDetailModel
    {
        return $this->request(
        'getOutboundOrderDetailItem',
        'GET',
        "api/cl-order/outbound_order_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundOrder resource.
     *
     * @param string $id      OutboundOrder identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'status'	string
     *                        'status[]'	array
     *                        'cartonId[between]'	string
     *                        'cartonId[gt]'	string
     *                        'cartonId[gte]'	string
     *                        'cartonId[lt]'	string
     *                        'cartonId[lte]'	string
     *                        'quantity[between]'	string
     *                        'quantity[gt]'	string
     *                        'quantity[gte]'	string
     *                        'quantity[lt]'	string
     *                        'quantity[lte]'	string
     *                        'createTime[before]'	string
     *                        'createTime[strictly_before]'	string
     *                        'createTime[after]'	string
     *                        'createTime[strictly_after]'	string
     *                        'updateTime[before]'	string
     *                        'updateTime[strictly_before]'	string
     *                        'updateTime[after]'	string
     *                        'updateTime[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *
     * @return OutboundOrderDetailModel[]|null
     */
    public function api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource',
        'GET',
        "api/cl-order/outbound_orders/$id/outbound_order_details",
        null,
        $queries,
        []
        );
    }
}
