<?php

namespace WarehouseX\ClOrder\Api;

use WarehouseX\ClOrder\Model\InboundOrderDetail as InboundOrderDetailModel;

class InboundOrderDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrderDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'status'	string
     *                       'status[]'	array
     *                       'inboundOrder.id'	integer
     *                       'inboundOrder.id[]'	array
     *                       'cartonId[between]'	string
     *                       'cartonId[gt]'	string
     *                       'cartonId[gte]'	string
     *                       'cartonId[lt]'	string
     *                       'cartonId[lte]'	string
     *                       'inboundOrder.id[between]'	string
     *                       'inboundOrder.id[gt]'	string
     *                       'inboundOrder.id[gte]'	string
     *                       'inboundOrder.id[lt]'	string
     *                       'inboundOrder.id[lte]'	string
     *                       'receiveTime[before]'	string
     *                       'receiveTime[strictly_before]'	string
     *                       'receiveTime[after]'	string
     *                       'receiveTime[strictly_after]'	string
     *                       'stockedTime[before]'	string
     *                       'stockedTime[strictly_before]'	string
     *                       'stockedTime[after]'	string
     *                       'stockedTime[strictly_after]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return InboundOrderDetailModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderDetailCollection',
        'GET',
        'api/cl-order/inbound_order_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a InboundOrderDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderDetailModel|null
     */
    public function getItem(string $id): ?InboundOrderDetailModel
    {
        return $this->request(
        'getInboundOrderDetailItem',
        'GET',
        "api/cl-order/inbound_order_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrder resource.
     *
     * @param string $id      InboundOrder identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'status'	string
     *                        'status[]'	array
     *                        'inboundOrder.id'	integer
     *                        'inboundOrder.id[]'	array
     *                        'cartonId[between]'	string
     *                        'cartonId[gt]'	string
     *                        'cartonId[gte]'	string
     *                        'cartonId[lt]'	string
     *                        'cartonId[lte]'	string
     *                        'inboundOrder.id[between]'	string
     *                        'inboundOrder.id[gt]'	string
     *                        'inboundOrder.id[gte]'	string
     *                        'inboundOrder.id[lt]'	string
     *                        'inboundOrder.id[lte]'	string
     *                        'receiveTime[before]'	string
     *                        'receiveTime[strictly_before]'	string
     *                        'receiveTime[after]'	string
     *                        'receiveTime[strictly_after]'	string
     *                        'stockedTime[before]'	string
     *                        'stockedTime[strictly_before]'	string
     *                        'stockedTime[after]'	string
     *                        'stockedTime[strictly_after]'	string
     *                        'createTime[before]'	string
     *                        'createTime[strictly_before]'	string
     *                        'createTime[after]'	string
     *                        'createTime[strictly_after]'	string
     *                        'updateTime[before]'	string
     *                        'updateTime[strictly_before]'	string
     *                        'updateTime[after]'	string
     *                        'updateTime[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *
     * @return InboundOrderDetailModel[]|null
     */
    public function api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource',
        'GET',
        "api/cl-order/inbound_orders/$id/inbound_order_details",
        null,
        $queries,
        []
        );
    }
}
