<?php

namespace WarehouseX\ClOrder\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $cartonId = null;

    public $cartonReference = null;

    public $cartonNote = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var int
     */
    public $quantityAvailable = null;

    /**
     * @var string
     */
    public $status = 'WAIT_OUTBOUND';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $outboundOrder = null;
}
