<?php

namespace WarehouseX\ClOrder\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrderDetail.
 */
class InboundOrderDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $cartonId = null;

    public $cartonReference = null;

    public $cartonNote = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var int
     */
    public $actualCartonQuantity = null;

    /**
     * @var int
     */
    public $stockedCartonQuantity = null;

    /**
     * @var string
     */
    public $status = 'WAIT_RECEIVE';

    /**
     * @var string|null
     */
    public $receiveTime = null;

    /**
     * @var string|null
     */
    public $stockedTime = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $inboundOrder = null;
}
