<?php

namespace WarehouseX\ClOrder\Model\InboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var string|null
     */
    public $trackingNumber = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $userId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string|null
     */
    public $warehouseName = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var int
     */
    public $actualCartonQuantity = null;

    /**
     * @var int
     */
    public $stockedCartonQuantity = null;

    /**
     * @var string|null
     */
    public $estimatedArrivalTime = null;

    /**
     * @var string|null
     */
    public $lastReceiveTime = null;

    /**
     * @var string|null
     */
    public $lastStockedTime = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'DRAFT';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
