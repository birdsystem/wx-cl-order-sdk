<?php

namespace WarehouseX\ClOrder\Model\InboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderPatch extends AbstractModel
{
    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'DRAFT';
}
