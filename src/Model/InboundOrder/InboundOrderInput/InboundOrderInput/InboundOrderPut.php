<?php

namespace WarehouseX\ClOrder\Model\InboundOrder\InboundOrderInput\InboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $estimatedArrivalTime = null;

    /**
     * @var string|null
     */
    public $trackingNumber = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var
     * \WarehouseX\ClOrder\Model\InboundOrderDetailInput\InboundOrderInput\InboundOrderPut[]
     */
    public $inboundOrderDetails = null;

    /**
     * @var string
     */
    public $status = null;
}
