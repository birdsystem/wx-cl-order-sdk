<?php

namespace WarehouseX\ClOrder\Model\OutboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundOrderPatch extends AbstractModel
{
    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'DRAFT';
}
