<?php

namespace WarehouseX\ClOrder\Model\OutboundOrder\OutboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var int
     */
    public $logisticsServiceId = null;

    /**
     * @var string|null
     */
    public $trackingNumber = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $companyName = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'DRAFT';

    /**
     * @var
     * \WarehouseX\ClOrder\Model\OutboundOrderDetail\OutboundOrderInput\OutboundOrderPut[]
     */
    public $outboundOrderDetails = null;
}
