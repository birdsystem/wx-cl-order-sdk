<?php

namespace WarehouseX\ClOrder\Model\OutboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundOrderOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $userId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string|null
     */
    public $warehouseName = null;

    /**
     * @var int
     */
    public $logisticsServiceId = null;

    /**
     * @var string|null
     */
    public $logisticsServiceName = null;

    /**
     * @var string|null
     */
    public $trackingNumber = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var int
     */
    public $totalCartonQuantity = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $companyName = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'DRAFT';

    /**
     * @var string|null
     */
    public $outboundTime = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
