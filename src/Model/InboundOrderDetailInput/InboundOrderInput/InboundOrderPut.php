<?php

namespace WarehouseX\ClOrder\Model\InboundOrderDetailInput\InboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class InboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var string
     */
    public $cartonNote = null;
}
