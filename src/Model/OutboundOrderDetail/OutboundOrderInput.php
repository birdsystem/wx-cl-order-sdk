<?php

namespace WarehouseX\ClOrder\Model\OutboundOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderInput extends AbstractModel
{
    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $quantity = null;
}
