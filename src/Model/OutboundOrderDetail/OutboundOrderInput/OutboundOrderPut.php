<?php

namespace WarehouseX\ClOrder\Model\OutboundOrderDetail\OutboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $quantity = null;
}
